from django.shortcuts import render
from datetime import datetime, date

#I Kade Hendrick Putra Kurniawan

mhs_name = 'I Kade Hendrick Putra Kurniawan'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 3, 11)
npm = '1706984612'
college = 'Faculty of Computer Science, University of Indonesia'
hoby = 'Editing'
description = 'I am proficient at communicating, building, and maintaining profesional relationships. Have good experience in leaderships, visual design, and cinematography'


def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'college':college, 'hoby':hoby, 'description': description}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
